---
ID_PROV: 86
ID_REG: 19
URLAlbo: http://www.comuneagira.gov.it/it-it/amministrazione/atti-pubblicazioni/albo-pretorio
URLTelegram: https://telegram.me/albopopagira
URLfacebook: https://www.facebook.com/albopopAgira
URLtwitter: https://twitter.com/albopop_agira
autoreNome: Giuseppe Ricceri
autoreURL: https://www.facebook.com/etanoox
date: '2018-06-22T14:14:06+02:00'
draft: false
feedRSS: http://feeds.feedburner.com/AlbopopAgira
nome: Agira
province: Enna
regioni: Sicilia
slug: ''
tags: []
tipologie: [comune]
title: Agira (Sicilia)
---